# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lux/version'

Gem::Specification.new do |spec|
  spec.name          = "lux"
  spec.version       = Lux::VERSION
  spec.authors       = ["Nick Townsend"]
  spec.email         = ["nick.townsend@mac.com"]

  spec.summary       = %q{Handy utilities for working with Docker}
  spec.homepage      = "https://github.com/townsen/lux"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'bin'
  spec.executables   << 'lux'
  spec.require_paths = ["lib"]

  spec.required_ruby_version = ">=2.3.0"

  spec.add_runtime_dependency "thor", "~> 0.19"
  spec.add_runtime_dependency "highline", "~> 1.7"
  spec.add_runtime_dependency "docker-api", "~> 1.22"
  spec.add_runtime_dependency "rake", "~> 10.0"

  spec.add_development_dependency "bundler", "~> 1.9"
end
