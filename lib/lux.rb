require 'highline/import'
require 'docker'
require 'uri'
require 'socket'
require 'json'

module Lux

  module_function

  # Determine the IPv4 address of the current Docker host
  # Used to avoid long reverse DNS lookups on .local names
  # Also useful when running Docker for macOS which runs on
  # a Unix socket but makes container ports available on localhost
  #
  # Returns an array of three items:
  # - the IPv4 address as a string or '127.0.0.1' if it was a Unix socket
  # - a URI object
  # - a hash containing the Docker version information
  # These are all nil if no Docker server is found
  #
  def dockerip
    begin
      info = Docker.version
      uri = URI.parse Docker.url
      case uri.scheme
      when 'unix'
        addr = '127.0.0.1'
      when 'tcp'
        Addrinfo.tcp(uri.host, uri.port).connect(timeout: 5) {|s|
          addr = s.remote_address.ip_address
          s.print "GET /version HTTP/1.0\r\nHost: localhost\r\n\r\n"
          response, emptyline, body = s.read.partition(/(\r\n){2}/)
          versioninfo = JSON.parse(body, symbolize_names: false)
        }
      else
        raise "Can't handle #{uri.scheme}"
      end
      return addr, uri, info
    rescue
      return nil, nil, nil
    end
  end

  # Get the current list of images and make a guess at which one it is...
  #
  def findimage image
    local_images = `docker images`.strip.split("\n")[1..-1].map{|l| l.gsub!(/^(\S+)\s+(\S+).*/,'\1:\2')}.sort
    matching_images = local_images.select{|l| l.include? image }
    if matching_images.size > 0
      if image.count(':') == 0 and image.count('/') > 0
        matching_image = matching_images.select{|l| l.end_with? ':latest' }.first
      end
      unless matching_image
        if matching_images.size == 1
          matching_image = matching_images[0]
        else
          matching_image = HighLine.choose do |menu|
            menu.header = 'List of matching (local) images'
            menu.choices(*matching_images)
            menu.choice('None of the above') { nil }
          end
          exit 2 unless matching_image
        end
      end
    else
      matching_image = image
    end
    return matching_image
  end

  # Return two elements:
  # - user name (defaults to current user), and
  # - a bash script setup command
  # Omit the secret password: -p $1$Ih5pLngr$/boiJHqKbQm9AP/QQyq0b1
  #
  def user_setup_cmd user = `id -nu`.strip
    [user, <<-COMMAND.gsub(/^\s*/,'').gsub(/\n/,' ; ')]
      useradd -M -d #{ENV['HOME']} -u #{Process.uid} -s #{ENV['SHELL']} #{user}
      mkdir -p /etc/sudoers.d
      echo "#{user} ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/#{user}
    COMMAND
  end

  def die msg, rc = 1
    error msg
    exit rc
  end

  def error msg
    HighLine.new(STDIN, STDERR).say HighLine.color(msg, HighLine::RED)
  end

  def info msg
    HighLine.new(STDIN, STDERR).say HighLine.color(msg, HighLine::YELLOW)
  end

end

# vim: ft=ruby sts=2 sw=2 ts=8
